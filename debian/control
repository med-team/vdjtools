Source: vdjtools
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           tony mancill <tmancill@debian.org>
Section: non-free/science
XS-Autobuild: no
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               gradle-debian-helper,
               javahelper,
               maven-repo-helper
Build-Depends-Indep: groovy,
                     libcommons-lang3-java,
                     libgpars-groovy-java,
                     libmilib-java,
                     libcommons-math3-java,
                     junit4
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/vdjtools
Vcs-Git: https://salsa.debian.org/med-team/vdjtools.git
Homepage: https://github.com/mikessh/vdjtools
Rules-Requires-Root: no

Package: libvdjtools-java
Architecture: all
Section: non-free/java
Depends: ${java:Depends},
         ${misc:Depends}
Description: Java library of vdjtools
 The VDJtools are interacting with the R statistics environment
 to characterize the constitution of the immune repertoire
 of B and T cells.
 This package provides the Java library.

Package: vdjtools
Architecture: all
Depends: ${misc:Depends},
         libvdjtools-java,
         default-jre
Recommends: r-cran-ape,
            r-cran-ggplot2,
            r-cran-gplots,
            r-cran-mass,
            r-cran-plotrix,
            r-cran-rcolorbrewer,
            r-cran-reshape,
            r-cran-reshape2,
            r-cran-scales,
            r-cran-gridbase,
            r-cran-gridextra,
            r-cran-circlize,
            r-cran-venndiagram,
            r-cran-ffield
Description: framework for post-analysis of B/T cell repertoires
 VDJtools is an open-source Java/Groovy-based framework designed
 to facilitate analysis of immune repertoire sequencing (RepSeq)
 data. VDJtools computes a wide set of statistics and is able to perform
 various forms of cross-sample analysis. Both comprehensive tabular
 output and publication-ready plots are provided.
 .
 The main aims of the VDJtools Project are:
 .
  * To ensure consistency between post-analysis methods and results
  * To save the time of bioinformaticians analyzing RepSeq data
  * To create an API framework facilitating development of new RepSeq
    analysis applications
  * To provide a simple enough command line tool so it could be used by
    immunologists and biologists with little computational background
